/* 
 * File:   main.c
 * Author: charles
 *
 * Created on September 14, 2015, 9:22 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

struct arg_struct {
    int sleepTime;
    int threadId;
};
struct arg_struct **args;

pthread_mutex_t lock;
int count;

void * doWork(void * index)
{
    int* i = (int*)index;
    struct timespec ts;
    ts.tv_sec = (int)(args[*i]->sleepTime) / (1000 * 1000);
    ts.tv_nsec = ((int)(args[*i]->sleepTime) % (1000 * 1000)) * 1000;    
    nanosleep(&ts, NULL);
    pthread_mutex_lock(&lock);
    if(count < 2)
    {
       printf("%d", args[*i]->threadId); 
    }
    else
    {
        printf("%u,", args[*i]->threadId);
    }
    count--;
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}    

/*
 * Main function 
 */
int main(int argc, char** argv) {   
    if (argc != 2)
    {
        return (EXIT_FAILURE);
    }
    
    int noThreads = atoi(argv[1]);
    int i, id[noThreads];
    int rc;
    count = noThreads;
    
    if( pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed");
        exit(-1);
    }
    
    pthread_t *thread_id = malloc(sizeof(pthread_t) * noThreads);
    args = malloc(noThreads * sizeof(struct arg_struct *));
    srand(time(NULL));
    
    for (i = 0; i < noThreads; i++)
    {        
        id[i] = i;
        args[i] = malloc(sizeof(struct arg_struct));
        // Random number between 0 and 5000;
        args[i]->sleepTime = rand() % 5000001;
        args[i]->threadId = i;
        rc = pthread_create(&thread_id[i], NULL, doWork, (void *)(id + i));
        if(rc)
        {
            printf("ERROR: Return code from pthread_create is %d \n", rc);
            exit(-1);
        }
    }
    
    for (i =0; i < noThreads; i++)
    {
        pthread_join(thread_id[i], NULL);
        free(args[i]);
    }
    free(args);
    free(thread_id);
    pthread_mutex_destroy(&lock);
    return (EXIT_SUCCESS);
}

